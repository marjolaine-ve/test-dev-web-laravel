<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Star extends Model
{
	protected $table = 'stars';

    protected $fillable = [
        'lastname',
        'firstname',
        'image_path',
        'description'
    ];
}