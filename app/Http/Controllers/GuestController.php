<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Star;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function stars()
    {
        $stars = Star::orderBy('lastname', 'ASC')->get();

        return view('pages.guest.stars', compact([
            'stars'
        ]));
    }
}
