<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Star;

class StarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stars = Star::orderBy('lastname', 'ASC')->get();

        return view('ressources.stars-index', compact([
            'stars'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ressources.stars-create-update');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Vérification des champs
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'image' => 'image|mimes:png,jpg,jpeg|max:2048'
        ]);

        // Création et stockage de l'image
        $imageName = null;
        if($request->image) {
            $imageName = time() . '-' . $request->image->getClientOriginalName();
            $request->image->move(public_path('images'), $imageName);
        }

        // Création de la star avec les éléments du formulaire + l'image_path
        $star = Star::create($request->all());
        $star->image_path = $imageName;

        // Sauvegarde de la star (ou non) et redirection
        if($star->save()) {
            return redirect()->route('stars.index')->with('success', "La star " . $star->firstname . " " . $star->lastname . " a correctement été enregistrée.");
        }
        return redirect()->route('stars.index')->with('error', "La star n'a pas pu être enregistrée.");
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $star = Star::where('id', $id)->first();

        return view('ressources.stars-show', compact('star'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $star = Star::where('id', $id)->first();

        return view('ressources.stars-create-update', compact('star'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Vérification des champs
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'image' => 'image|mimes:png,jpg,jpeg|max:2048'
        ]);
        
        // Récupération et MAJ de la star
        $star = Star::where('id', $id)->first();
        $star->update($request->all());

        // Création et stockage de l'image
        $imageName = null;
        if($request->image) {
            $imageName = time() . '-' . $request->image->getClientOriginalName();
            $request->image->move(public_path('images'), $imageName);
        }
        $star->image_path = $imageName;

        // Sauvegarde de la star (ou non) et redirection
        if($star->save()) {
            return redirect()->route('stars.index')->with('success', "La star " . $star->firstname . " " . $star->lastname . " a correctement été modifiée.");
        }
        return redirect()->route('stars.index')->with('error', "La star n'a pas pu être modifiée.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $star = Star::where('id', $id)->first();
       
        if($star->delete()) {
            return redirect()->route('stars.index')->with('success', "La star a bien été supprimée !");
        }
        return redirect()->route('stars.index')->with('error', "La star n'a pas pu être supprimée.");
    }
}
