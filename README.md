# Test Dev Web Laravel

Projet créé en février 2023 pour un test technique sur Laravel.

Par : @marjolaine-ve

## Consignes
_À l'aide du framework PHP Laravel 9, créer :_
- Un backoffice permettant de créer/modifier/supprimer les fiches « star » (nom, prénom, image, description)
- Une page publique permettant d’afficher de manière responsive sur mobile et desktop le contenu des fiches.

_Indications :_
- L'utilisation de VueJS 3 et de TailwindCSS est un plus
- N'hésitez pas à commenter votre code et créer des commits sur git au fur et à mesure de votre progression

_Livrables attendus :_
- Lien github/gitlab contenant le projet, et un fichier « Readme » expliquant les étapes d'installation

## Installation

### Requis
- composer
- npm
- PHP >=8.0

### Étapes
1. Cloner le repo (`git clone git@gitlab.com:marjolaine-ve/test-dev-web-laravel.git`)
2. Aller dans le dossier cloné (`cd test-dev-web-laravel`)
3. Installer les dépendances (`composer install`)
4. Créer votre base de données dans votre SGBD en utf8 (ex : phpmyadmin)
5. Dupliquer le fichier `.env.example`, le renommer `.env` et mettre les informations de votre base de données
6. Générer la key Laravel (`php artisan key:generate`)
7. Générer vos tables de BDD (`php artisan migrate`)
8. Installer TailwindCSS (`npm install -D tailwindcss postcss autoprefixer`)
9. Lancer `npm run dev`
10. Lancer le serveur `php artisan serve`
11. Se rendre sur l'url avec le navigateur (ex : `localhost:8000`)
12. Se créer un compte pour accéder à l'interface admin

Vous pouvez à présent créer des "stars", les modifier, les supprimer, les voir... Et consulter la page public du listing des stars sur /stars !