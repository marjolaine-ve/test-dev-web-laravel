<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Stars') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if ($errors->any())
                <div class="p-5 mb-10 bg-rose-200 border border-rose-300 sm:rounded-lg">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if ($message = Session::get('success'))
                <div class="p-5 mb-10 bg-emerald-200 border border-emerald-300 sm:rounded-lg">
                    <p>{{ $message }}</p>
                </div>
            @endif
            
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6">
                    <h3 class="text-xl">{{ $star->firstname }} {{ $star->lastname }}</h3>
                </div>
                @if($star->image_path)
                    <div class="px-6 pb-6">
                        <img class="max-w-xs" src="{{ asset('images/'. $star->image_path) }}" />
                    </div>
                @endif
                <div class="px-6 text-gray-900">
                    {!! nl2br($star->description) !!}
                </div>
                <div class="p-6">
                    <a href="{{ route('stars.index') }}" class="button inline-flex justify-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2" title="Retourner à la liste des stars">Retour à la liste</a>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
