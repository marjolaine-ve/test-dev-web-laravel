<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Stars') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
			@if ($errors->any())
				<div class="p-5 mb-10 bg-rose-200 border border-rose-300 sm:rounded-lg">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

			@if ($message = Session::get('success'))
        		<div class="p-5 mb-10 bg-emerald-200 border border-emerald-300 sm:rounded-lg">
					<p>{{ $message }}</p>
				</div>
			@endif

            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
				<div class="p-6 text-gray-900">
					<div>
						<div class="md:grid md:grid-cols-3 md:gap-6">
							<div class="md:col-span-1">
								<div class="px-4 sm:px-0">
									<h3 class="mb-4 text-lg font-medium leading-6 text-gray-900">
										@if(isset($star))
											Édition du profil de star de {{ $star->firstname }} {{ $star->lastname }}
										@else
											Création du profil d'une star
										@endif
									</h3>
								</div>
							</div>
						</div>
						<div class="mt-10 md:col-span-2 md:mt-0">
							@if(isset($star))

								<form method="POST" enctype="multipart/form-data" action="{{ route('stars.update', $star->id) }}">
						            @method('PUT')
							@else
								<form method="POST" enctype="multipart/form-data" action="{{ route('stars.store') }}">
							@endif
						    @csrf
          						<div class="space-y-6 bg-white py-5 sm:p-6">
									<div>
										<label for="lastname" class="block text-sm font-medium text-gray-700">Nom de famille<sup>*</sup> :</label>
										<div class="relative mt-1 rounded-md shadow-sm">
											<input type="text" name="lastname" id="lastname" class="block w-full rounded-md border-gray-300 pl-7 pr-12 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm" placeholder="" value="@if(isset($star)){{ $star->lastname }}@endif">
										</div>
									</div>

									<div>
										<label for="firstname" class="block text-sm font-medium text-gray-700">Prénom<sup>*</sup> :</label>
										<div class="relative mt-1 rounded-md shadow-sm">
											<input type="text" name="firstname" id="firstname" class="block w-full rounded-md border-gray-300 pl-7 pr-12 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm" placeholder="" value="@if(isset($star)){{ $star->firstname }}@endif">
										</div>
									</div>

									<div>
										<label class="block text-sm font-medium text-gray-700">Image</label>
										<div class="mt-1 flex items-center">
											@if(!isset($star) || (isset($star) && empty($star->image_path)))
												<span class="inline-block h-12 w-12 overflow-hidden rounded-full bg-gray-100">
													<svg class="h-full w-full text-gray-300" fill="currentColor" viewBox="0 0 24 24">
														<path d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z" />
													</svg>
												</span>
											@else
												<img class="max-w-xs" src="{{ asset('images/'. $star->image_path) }}" />
											@endif
											<input type="file" class="ml-5 rounded-md border border-gray-300 bg-white py-2 px-3 text-sm font-medium leading-4 text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2" name="image" />
										</div>
									</div>

									<div>
										<label for="description" class="block text-sm font-medium text-gray-700">{{ __('Description') }}</label>
										<div class="mt-1">
											<textarea id="description" name="description" rows="8" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm" placeholder="">@if(isset($star)){{ $star->description }}@endif</textarea>
										</div>
									</div>
								</div>
								<div class="pt-4">
									<button type="submit" class="inline-flex justify-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">Enregistrer</button>
								</div>

							</form>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
