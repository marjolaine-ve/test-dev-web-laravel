<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Stars') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
			@if ($errors->any())
				<div class="p-5 mb-10 bg-rose-200 border border-rose-300 sm:rounded-lg">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

			@if ($message = Session::get('success'))
        		<div class="p-5 mb-10 bg-emerald-200 border border-emerald-300 sm:rounded-lg">
					<p>{{ $message }}</p>
				</div>
			@endif
			
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
        		<div class="p-6 flex justify-between">
        			<h3 class="text-xl">Liste des stars</h3>
					<a href="{{ route('stars.create') }}" class="button inline-flex justify-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2" title="Ajouter une star">Ajouter une star</a>
        		</div>
                <div class="p-6 text-gray-900">
                	<table class="w-full text-sm text-gray-500 dark:text-gray-400 table-fixed">
                		<thead class="text-xs text-gray-700 uppercase bg-gray-50">
                			<tr class="border-b">
                				<th class="p-2 text-left">{{ __('Nom') }}</th>
                				<th class="p-2 text-left">{{ __('Prénom') }}</th>
                				<th class="p-2 text-center ">{{ __('Actions') }}</th>
                			</tr>
                		</thead>
                		<tbody>
                			@if(isset($stars) && count($stars) > 0)
                				@foreach($stars as $star)
                					<tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                						<td class="p-2 text-left">{{ $star->lastname }}</td>
                						<td class="p-2 text-left">{{ $star->firstname }}</td>
                						<td class="p-2 text-center">
                							<a href="{{ route('stars.show', $star->id) }}" class="inline-flex items-center px-2 py-1 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring ring-gray-300 disabled:opacity-25 transition ease-in-out duration-150 ml-3" title="Voir la star">Voir</a>
                							<a href="{{ route('stars.edit', $star->id) }}" class="inline-flex items-center px-2 py-1 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring ring-gray-300 disabled:opacity-25 transition ease-in-out duration-150 ml-3" title="Modifier la star">Modifier</a>
											<form action="{{ route('stars.destroy', $star->id) }}" method="POST" style="display: inline;">
												@csrf
												@method('DELETE')
												<button type="submit" class="inline-flex items-center px-2 py-1 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring ring-gray-300 disabled:opacity-25 transition ease-in-out duration-150 ml-3">Supprimer</button>
											</form>
                						</td>
                					</tr>
                				@endforeach
            				@else
            					<tr><td cospla="3">Aucune star en base de données</td></tr>
        					@endif
        				</tbody>
        			</table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
