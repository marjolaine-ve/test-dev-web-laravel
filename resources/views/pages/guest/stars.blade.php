<x-guest-page-layout>
	<div class="min-h-screen flex flex-col items-center pt-6 sm:pt-0 bg-gray-100">
	    <div class="py-12 max-w-7-xl w-full">
	        <div class="sm:py-4 max-w-7xl mx-auto sm:px-6 lg:px-8 w-full">
	        	<h1 class="text-xl pb-8 px-4 sm:px-0">Liste des stars</h1>

	        	@if(count($stars) > 0)
					<div class="accordion w-full" id="accordionStar">
						@foreach($stars as $star)
							<div class="accordion-item w-full">
								<h2 class="accordion-header mb-0 w-full bg-gray-500" id="heading-{{ $star->id }}">
									<button class="accordion-button relative flex items-center w-full px-2 py-1 text-base text-gray-200 text-left border-0 rounded-none transition focus:outline-none sm:px-5 sm:py-4" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-{{ $star->id }}" aria-expanded="true" aria-controls="collapse-{{ $star->id }}">
										{{ $star->lastname }} {{ $star->firstname }}
									</button>
								</h2>
								<div id="collapse-{{ $star->id }}" class="accordion-collapse text-gray-800 bg-white" aria-labelledby="heading-{{ $star->id }}" data-bs-parent="#accordionStar">
									<div class="accordion-body py-4 px-5">
						                @if($star->image_path)
						                    <div class="pb-6 float-left sm:pr-6">
						                        <img class="max-w-none w-full sm:max-w-xs" src="{{ asset('images/'. $star->image_path) }}" />
						                    </div>
						                @endif
										<h2 class="text-xl">{{ $star->lastname }} {{ $star->firstname }}</h2>
										@if($star->description){!! nl2br($star->description) !!}@else<em>Description à venir</em>@endif
									</div>
								</div>
							</div>
						@endforeach
					</div>
				@else
					<p class="py-6">Aucune star n'est actuellement enregistrée en base de données.</p>
				@endif
	        </div>
	    </div>
	</div>
</x-guest-page-layout>