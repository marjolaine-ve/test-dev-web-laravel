import './bootstrap';

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();


// Sur les pages où il y a l'élément #accordionStar
if($('#accordionStar').length) {

	// Au chargement de la page
	openAccordionOnLoadedPageIfNotOnSmartphone();

	// Au clic sur le header d'un accordéon pour l'ouvrir / le fermer
	$('#accordionStar .accordion-header').on('click', function(e) {
		e.preventDefault();
		openAccordionElement($(this));
	})

	// Au resize de la page, si on passe de smartphone à tablette/ordi et qu'aucun élément n'est ouvert, il faut en ouvrir un
	window.onresize = function(event) {
		if(!isSmartphone() && $('#accordionStar .accordion-item.opened').length == 0) {
			openAccordionOnLoadedPageIfNotOnSmartphone();
		}
	};
}



/**********************************************************************************/
// FUNCTIONS
//
// isSmartphone()
// openAccordionElement(element)
// openAccordionOnLoadedPageIfNotOnSmartphone()
/**********************************************************************************/
function isSmartphone() {
	return ($(window).width() <= 640 ? true : false);
}


function openAccordionElement(element) {

	// Sur smartphone : possibilité de fermer l'élément ouvert
	if(isSmartphone()) {
		if(element.parent().hasClass('opened')) {
			$('.accordion-item').removeClass('opened');
		}
		else {
			$('.accordion-item').removeClass('opened');
			element.parent().addClass('opened');
		}
	}
	// Sur ordi / tablette : toujours laisser un élément ouvert
	else {
		$('.accordion-item').removeClass('opened');
		element.parent().addClass('opened');
	}
}


function openAccordionOnLoadedPageIfNotOnSmartphone() {

	// Ouvre par défaut le premier élément de l'accordéon
	if(!isSmartphone()) {
		openAccordionElement($('#accordionStar .accordion-item:first-child .accordion-header'));
	}
}
